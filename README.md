# OpenML dataset: monks-problems-3

https://www.openml.org/d/335

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Sebastian Thrun (Carnegie Mellon University)  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/MONK's+Problems) - October 1992  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)   

**The Monk's Problems: Problem 3**  
Once upon a time, in July 1991, the monks of Corsendonk Priory were faced with a school held in their priory, namely the 2nd European Summer School on Machine Learning. After listening more than one week to a wide variety of learning algorithms, they felt rather confused: Which algorithm would be optimal? And which one to avoid? As a consequence of this dilemma, they created a simple task on which all learning algorithms ought to be compared: the three MONK's problems.

The target concept associated with the 3rd Monk's problem is the binary outcome of the logical formula:  
MONK-3: (a5 = 3 and a4 = 1) or (a5 /= 4 and a2 /= 3)  
In addition, 5% class noise was added to the training set

In this dataset, the original train and test sets were merged to allow other sampling procedures. However, the original train-test splits can be found as one of the OpenML tasks. 

### Attribute information: 
* attr1: 1, 2, 3 
* attr2: 1, 2, 3 
* attr3: 1, 2 
* attr4: 1, 2, 3 
* attr5: 1, 2, 3, 4 
* attr6: 1, 2 

### Relevant papers  
The MONK's Problems - A Performance Comparison of Different Learning Algorithms, by S.B. Thrun, J. Bala, E. Bloedorn, I. Bratko, B. Cestnik, J. Cheng, K. De Jong, S. Dzeroski, S.E. Fahlman, D. Fisher, R. Hamann, K. Kaufman, S. Keller, I. Kononenko, J. Kreuziger, R.S. Michalski, T. Mitchell, P. Pachowicz, Y. Reich H. Vafaie, W. Van de Welde, W. Wenzel, J. Wnek, and J. Zhang. Technical Report CS-CMU-91-197, Carnegie Mellon University, Dec. 1991.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/335) of an [OpenML dataset](https://www.openml.org/d/335). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/335/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/335/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/335/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

